Card guessing game. 
Each round players must guess if next card drawn from the deck is equal, higher or lower to the base card. 
Card ranks are from 2 to 10. Jack, Queen, King and Ace have rank 10. 