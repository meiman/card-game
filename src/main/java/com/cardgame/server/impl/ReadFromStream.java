package com.cardgame.server.impl;

import java.io.DataInputStream;
import java.util.concurrent.Callable;

public class ReadFromStream implements Callable<String> {
  private final DataInputStream inputStream;

  ReadFromStream(DataInputStream inputStream) {
    this.inputStream = inputStream;
  }

  @Override
  public String call() throws Exception {
    try {
      while (true)  {
        if (inputStream.available()>0)  {
          System.out.println("Waiting " + inputStream.available());
          return inputStream.readUTF();
        }
        Thread.sleep(200);
      }
    } catch (InterruptedException e)  {
      return "";
    }
  }
}
