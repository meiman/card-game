package com.cardgame.server.impl;

import com.cardgame.common.Card;
import com.cardgame.common.PlayerAction;
import com.cardgame.game.protocol.FinishRoundRequest;
import com.cardgame.game.protocol.PlayerActionRequest;
import com.cardgame.game.protocol.PlayerActionResponse;
import com.cardgame.game.protocol.StartRoundRequest;
import com.cardgame.server.api.GameService;
import com.cardgame.server.api.SetBaseCardRequest;
import com.cardgame.server.api.SetBaseCardResponse;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

public class GameServiceImpl implements GameService, Runnable {
  private final Socket socket;
  private DataInputStream inStream;
  private final DataOutputStream outStream;

  private List<Card> deck;

  private boolean clientActive;

  private int roundID;
  private long timestamp;
  private final int roundDuration = 10;
  private Card currentCard;
  private Card previousCard;

  public GameServiceImpl(Socket sock, boolean notForTesting) throws IOException {
    this.socket = sock;
    this.inStream = new DataInputStream(sock.getInputStream());
    this.outStream = new DataOutputStream(sock.getOutputStream());
    clientActive = notForTesting;
    roundID = 0;

    System.out.println();
    System.out.println("New server thread");

    deck = CardDeck.getNewRandomDeck();
    setNewCard();
  }

  /**
   * Main game loop, runs until client quits or connection is lost
   */
  void runGame() throws IOException {
    while (clientActive)  {

      if (deck.isEmpty()) {
        deck = CardDeck.getNewRandomDeck();
      }

      startRound();

      // Get action from client
      PlayerAction playerAction = waitForActionOrTimeout();
      System.out.println("Player action: " + playerAction);
      if (!clientActive)  return;

      if (playerAction!=null) {
        System.out.println("Send response");
        PlayerActionResponse response = playerAction(new PlayerActionRequest(playerAction));
        sendString(response.toString());

        if (playerAction.equals(PlayerAction.QUIT)) {
          System.out.println("Client disconnected");
          break;
        }
      }

      // Take new card
      previousCard = currentCard;
      setNewCard();

      // Finish round
      finishRound(playerAction);
    }
  }

  private void sendString(String request) throws IOException {
    outStream.writeUTF(request);
  }

  private PlayerAction waitForActionOrTimeout() {
    ExecutorService executor = Executors.newSingleThreadExecutor();
    //Future<String> readWithTimeout = executor.submit(new ReadFromStream(inStream));
    Future<String> readWithTimeout = executor.submit(new ReadFromStream(inStream));
    String clientAction;
    PlayerAction playerAction = null;
    try {
      // give the client 10 seconds to return a playerAction, after 10s expect timeout
      clientAction = readWithTimeout.get(roundDuration, TimeUnit.SECONDS);
      playerAction = PlayerAction.valueOf(clientAction);
      System.out.println("Got input: " + playerAction);
    } catch (TimeoutException e)  {
      System.out.println("Client timeout");
    } catch (InterruptedException e) {
      e.printStackTrace();
    } catch (ExecutionException e) {
      e.printStackTrace();
      // Connection lost
      clientActive=false;
    } finally {
      readWithTimeout.cancel(true);
      executor.shutdownNow();
    }

    return playerAction;
  }

  private boolean isPlayerWinner(PlayerAction playerAction)  {
    if (playerAction==null) return false;
    PlayerAction correctGuess;
    if (previousCard.getValue().compareTo(currentCard.getValue()) > 0) {
      correctGuess = PlayerAction.LOWER;
    } else if (previousCard.getValue().compareTo(currentCard.getValue()) < 0)  {
      correctGuess = PlayerAction.HIGHER;
    } else {
      correctGuess = PlayerAction.EQUALS;
    }
    return playerAction.equals(correctGuess);
  }

  private void setNewCard() {
    currentCard = deck.remove(0);
  }

  @Override
  public void startRound() {
    timestamp = System.currentTimeMillis();
    roundID = ThreadLocalRandom.current().nextInt(0, 10000);
    System.out.println("Round " +roundID+ " started at " +timestamp);
    StartRoundRequest startRoundRequest = new StartRoundRequest(roundDuration, timestamp, roundID, currentCard);
    try {
      sendString(startRoundRequest.toString());
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void finishRound(PlayerAction playerAction) {
    System.out.println("Round " +roundID+ " finished");
    boolean isWinner = isPlayerWinner(playerAction);
    FinishRoundRequest finishRoundRequest = new FinishRoundRequest(roundID, isWinner);
    try {
      sendString(finishRoundRequest.toString());
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Override
  public PlayerActionResponse playerAction(PlayerActionRequest playerActionRequest) {
    if (Arrays.asList(PlayerAction.values()).contains(playerActionRequest.getPlayerAction())) {
      return new PlayerActionResponse("OK");
    }
    else return new PlayerActionResponse("No such action" + playerActionRequest.getPlayerAction());
  }

  @Override
  public SetBaseCardResponse setBaseCard(SetBaseCardRequest setBaseCardRequest) {
    if (Arrays.asList(Card.values()).contains(setBaseCardRequest.getBaseCard()))  {
      return new SetBaseCardResponse("OK");
    }
    else return new SetBaseCardResponse("No such card: "+setBaseCardRequest.getBaseCard());
  }

  @Override
  public void run() {
    try {
      if (!clientActive)  {
        //String input = new ReadFromStream(inStream).call();
        String input = new ReadFromStream(inStream).call();
        SetBaseCardResponse response = setBaseCard(new SetBaseCardRequest(Card.valueOf(input)));
        sendString(response.toString());
        clientActive = true;
      }
      runGame();
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
        System.out.println("Server thread shutdown");
        inStream.close();
        outStream.close();
        socket.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }
}
