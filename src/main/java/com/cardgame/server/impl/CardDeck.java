package com.cardgame.server.impl;

import com.cardgame.common.Card;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CardDeck {
  public static List<Card> getNewRandomDeck() {
    SecureRandom secureRandom = null;
    try {
      secureRandom = SecureRandom.getInstance("SHA1PRNG");
    } catch (NoSuchAlgorithmException e) {
      secureRandom = new SecureRandom();
    }

    List<Card> orderedDeck = new ArrayList<>(Arrays.asList(Card.values()));
    Collections.shuffle(orderedDeck, secureRandom);
    return orderedDeck;
  }
}
