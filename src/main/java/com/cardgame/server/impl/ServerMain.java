package com.cardgame.server.impl;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerMain {
  private static int port;

  public static void main(String[] args) throws IOException {
    try {
      port = Integer.parseInt(args[0]);
    } catch (Exception e) {
      System.out.println("Invalid arguments, try: server <port>");
      System.exit(1);
    }

    try (ServerSocket ss = new ServerSocket(port))  {
      while (true)    {
        System.out.println("Listening on port " + port + "...");

        // Wait for connection
        Socket sock = ss.accept();
        // Create a thread to handle the connection
        new Thread(new GameServiceImpl(sock, true)).start();
      }
    }
  }
}
