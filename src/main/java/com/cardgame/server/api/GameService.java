package com.cardgame.server.api;

import com.cardgame.common.PlayerAction;
import com.cardgame.game.protocol.PlayerActionRequest;
import com.cardgame.game.protocol.PlayerActionResponse;

/**
 * This is a game server that will receive messages from client
 */
public interface GameService {
  /**
   * Client notifies server of its action
   */
  PlayerActionResponse playerAction(PlayerActionRequest playerActionRequest);

  /**
   * Client manually sets a base card. To be used only for testing purpose!
   * Should only be called outside active game round and return an error if game round is already active.
   */
  SetBaseCardResponse setBaseCard(SetBaseCardRequest setBaseCardRequest);

  /**
   * Checks if player wins and send finishRoundRequest to client
   */
  void finishRound(PlayerAction playerAction);

  /**
   * Sends startRoundRequest to client
   */
  void startRound();
}
