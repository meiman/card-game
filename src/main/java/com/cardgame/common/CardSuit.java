package com.cardgame.common;

public enum CardSuit {
    CLUBS,
    SPADES,
    DIAMONDS,
    HEARTS
}
