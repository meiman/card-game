package com.cardgame.common;

public enum PlayerAction {
    HIGHER,
    LOWER,
    EQUALS,
    QUIT;
}
