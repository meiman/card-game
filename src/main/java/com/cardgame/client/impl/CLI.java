package com.cardgame.client.impl;

import com.cardgame.common.PlayerAction;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class CLI implements Callable<PlayerAction> {
  private final int timeout;

  CLI(int timeout) {
    this.timeout = timeout;
  }

  private PlayerAction waitForInput() throws IOException {

    ScheduledExecutorService scheduledExecutor = Executors.newScheduledThreadPool(0);
    displayCountdown(scheduledExecutor);

    String userInput = "";
    PlayerAction playerAction=null;
    boolean allowInput = true;

    BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
      while (userInput.equals("") || allowInput)  {
        System.out.println("Choose if the next card will be higher(h), lower(l), equal(e) or quit game(q)");
        try {
          while (!in.ready()) {
            Thread.sleep(200); // allow interrupts
          }
          userInput = in.readLine();
          playerAction = getActionBasedOnInput(userInput);
          if (playerAction == null) {
            System.out.println("No such command, try h, l, e or q");
            allowInput = true;
          }
          else allowInput = false;
        } catch (InterruptedException e) {
          scheduledExecutor.shutdownNow();
          return null;
        }
      }

    scheduledExecutor.shutdownNow();
    return playerAction;
  }

  private PlayerAction getActionBasedOnInput(String input)  {
    switch (input) {
      case "h":
        return PlayerAction.HIGHER;
      case "l":
        return PlayerAction.LOWER;
      case "e":
        return PlayerAction.EQUALS;
      case "q":
        return PlayerAction.QUIT;
      default:
        return null;
    }
  }

  private void displayCountdown(ScheduledExecutorService executorService) {
    executorService.scheduleAtFixedRate(new Runnable() {
      int counter=timeout;
      @Override
      public void run() {
        System.out.println(counter);
        counter--;
        if (counter==0) executorService.shutdown();
      }
    }, 0, 1, TimeUnit.SECONDS);
  }

  @Override
  public PlayerAction call() {
    try {
      return waitForInput();
    } catch (IOException e) {
      e.printStackTrace();
    }

    return null;
  }
}
