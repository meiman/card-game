package com.cardgame.client.impl;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class ClientMain {
  private static String HOSTNAME;
  private static int PORT;

  public static void main(String[] args) throws IOException {
    try {
      HOSTNAME = args[0];
      PORT = Integer.parseInt(args[1]);
    } catch (Exception e) {
      System.out.println("Invalid arguments, try: client <hostname> <port>");
      System.exit(1);
    }

    try (
            Socket socket = new Socket(HOSTNAME, PORT);
            DataInputStream inputStream = new DataInputStream(socket.getInputStream());
            DataOutputStream outputStream = new DataOutputStream(socket.getOutputStream());
    ) {
      System.out.println("Connected to server");
      System.out.println();
      GameClientImpl clientService = new GameClientImpl(inputStream, outputStream);

      clientService.runGame();
    }
  }

}
