package com.cardgame.client.impl;

import com.cardgame.client.api.GameClient;
import com.cardgame.common.Card;
import com.cardgame.common.PlayerAction;
import com.cardgame.game.protocol.*;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.concurrent.*;

public class GameClientImpl implements GameClient {
  private final DataInputStream inStream;
  private final DataOutputStream outStream;

  private int roundDuration;
  private long roundID;
  private long timestamp;
  private Card currentCard;
  private Card previousCard;

  public GameClientImpl(DataInputStream in, DataOutputStream out) {
    this.inStream = in;
    this.outStream = out;
  }

  /**
   * Game loop, runs until user inputs 'q'
   */
  public void runGame() {
    while (true)  {
      // Get start round parameters and start round
      startRound((StartRoundRequest) processInput());

      // Get user action
      PlayerAction playerAction = getActionFromPlayer();

      System.out.println(playerAction);

      if (playerAction!=null) {
        // Send action to server and expect a response
        sendString(playerAction.toString());

        PlayerActionResponse response = (PlayerActionResponse) processInput();
        System.out.println("Server responded: " + response.getErrorText());

        // Quit
        if (playerAction.equals(PlayerAction.QUIT)) {
          System.out.println("Game Over!");
          break;
        }
      }

      // Get finish round parameters and finish round
      finishRound((FinishRoundRequest) processInput());
    }
  }

  public ProtocolObject processInput() {
    String[] params = receiveString().split(",");

    Command command = Command.valueOf(params[0]);
    switch (command)  {
      case START: {
        return new StartRoundRequest(Integer.parseInt(params[1]), Long.parseLong(params[2]), Long.parseLong(params[3]), Card.valueOf(params[4]));
      }
      case FINISH:  {
        return new FinishRoundRequest(Integer.parseInt(params[1]), Boolean.parseBoolean(params[2]));
      }
      case ACTIONRESPONSE:  {
        return new PlayerActionResponse(params[1]);
      }
      default:  {
        // Communication error
        return null;
      }
    }
  }

  public PlayerAction getActionFromPlayer()  {
    PlayerAction playerAction=null;

    Future<PlayerAction> cliResponse;
    ExecutorService executor = Executors.newSingleThreadExecutor();

    CLI cli = new CLI(roundDuration);
    cliResponse = executor.submit(cli);

    try {
      playerAction = cliResponse.get(roundDuration, TimeUnit.SECONDS);
      System.out.println("Got input " + playerAction);
    } catch (TimeoutException e) {
      System.out.println("Time's up!");
    } catch (InterruptedException | ExecutionException e) {
      e.printStackTrace();
    } finally {
      cliResponse.cancel(true);
      executor.shutdownNow();
    }

    return playerAction;
  }

  private String receiveString() {
    try {
      return inStream.readUTF();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return "";
  }

  private void sendString(String request) {
    try {
      outStream.writeUTF(request);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void startRound(StartRoundRequest startRoundRequest) {
    roundDuration = startRoundRequest.getActionRoundDuration();
    timestamp = startRoundRequest.getActionRoundStartTimestamp();
    roundID = startRoundRequest.getRoundId();
    currentCard = startRoundRequest.getBaseCard();

    System.out.print("Round "+roundID+" started, current card is " + currentCard);
    if (previousCard!=null) System.out.println(" previous card was " + previousCard);
    else System.out.println();
  }

  @Override
  public void finishRound(FinishRoundRequest finishRoundRequest) {
    System.out.println("Round finished");
    if (finishRoundRequest.isWin()) System.out.println("You win");
    else System.out.println("You lose");
    System.out.println();
    previousCard = currentCard;
  }


  // For testing

  public int getRoundDuration() {
    return roundDuration;
  }

  public long getRoundID() {
    return roundID;
  }

  public long getTimestamp() {
    return timestamp;
  }

  public Card getCurrentCard() {
    return currentCard;
  }

  public void setRoundDuration(int roundDuration) {
    this.roundDuration = roundDuration;
  }
}
