package com.cardgame.game.protocol;

import com.cardgame.common.PlayerAction;

import java.io.Serializable;

/**
 * Player notifies server about its action
 */
public final class PlayerActionRequest implements Serializable, ProtocolObject {
    private final PlayerAction playerAction;
    private final Command command = Command.ACTIONREQUEST;

    public PlayerActionRequest(PlayerAction playerAction) {
        this.playerAction = playerAction;
    }

    public PlayerAction getPlayerAction() {
        return playerAction;
    }

    @Override
    public String toString() {
        return command+","+playerAction+"";
    }
}
