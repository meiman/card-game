package com.cardgame.game.protocol;

import com.cardgame.common.Card;

import java.io.Serializable;

/**
 * Server notifies client that round has started
 * Server provides client will all the necessary information that client can use to display to player
 */
public final class StartRoundRequest implements Serializable, ProtocolObject {
    private final int actionRoundDuration;
    private final long actionRoundStartTimestamp;
    private final long roundId;
    private final Card baseCard;
    private final Command command = Command.START;

    public StartRoundRequest(int actionRoundDuration, long actionRoundStartTimestamp, long roundId, Card baseCard) {
        this.actionRoundDuration = actionRoundDuration;
        this.actionRoundStartTimestamp = actionRoundStartTimestamp;
        this.roundId = roundId;
        this.baseCard = baseCard;
    }

    public int getActionRoundDuration() {
        return actionRoundDuration;
    }

    public long getActionRoundStartTimestamp() {
        return actionRoundStartTimestamp;
    }

    public long getRoundId() {
        return roundId;
    }

    public Card getBaseCard() {
        return baseCard;
    }

    @Override
    public String toString() {
        return command+","+actionRoundDuration + "," + actionRoundStartTimestamp + "," + roundId + "," + baseCard;
    }
}
