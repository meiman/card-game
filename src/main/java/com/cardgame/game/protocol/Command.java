package com.cardgame.game.protocol;

public enum Command {
  START,
  FINISH,
  ACTIONREQUEST,
  ACTIONRESPONSE
}
