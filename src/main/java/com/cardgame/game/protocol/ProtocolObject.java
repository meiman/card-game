package com.cardgame.game.protocol;

public interface ProtocolObject {
  public String toString();
}
