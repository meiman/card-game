package com.cardgame.game.protocol;

import java.io.Serializable;

/**
 * Notifies client is PlayerActionRequest was successful or not
 * ErrorText - meaningful error text in case there was an error. OK if no error.
 */
public class PlayerActionResponse implements Serializable, ProtocolObject {
    private final String errorText;
    private final Command command = Command.ACTIONRESPONSE;

    public PlayerActionResponse(String errorText) {
        this.errorText = errorText;
    }

    public String getErrorText() {
        return errorText;
    }

    @Override
    public String toString() {
        return command+","+errorText+"";
    }
}
