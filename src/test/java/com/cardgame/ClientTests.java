package com.cardgame;

import com.cardgame.client.impl.GameClientImpl;
import com.cardgame.common.Card;
import com.cardgame.common.PlayerAction;
import com.cardgame.game.protocol.FinishRoundRequest;
import com.cardgame.game.protocol.StartRoundRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ClientTests {
  private final int roundduration = 10;

  private GameClientImpl client;
  @BeforeEach
  public void createClient()  {
    client = new GameClientImpl(new DataInputStream(System.in), new DataOutputStream(System.out));
    client.setRoundDuration(roundduration);
  }

  @Test
  public void testStartRound()  {
    StartRoundRequest startRoundRequest = new StartRoundRequest(roundduration, 1000, 1, Card.C2);

    client.startRound(startRoundRequest);

    assertEquals(roundduration, client.getRoundDuration());
    assertEquals(1000, client.getTimestamp());
    assertEquals(1, client.getRoundID());
    assertEquals(Card.C2, client.getCurrentCard());
  }

  @Test
  public void finishRoundWin() {
    OutputStream messageToUser = new ByteArrayOutputStream();
    System.setOut(new PrintStream(messageToUser));

    FinishRoundRequest finishRoundRequest = new FinishRoundRequest(1, true);
    client.finishRound(finishRoundRequest);
    assertEquals("Round finishedYou win", messageToUser.toString().trim().replace("\n", "").replace("\r", ""));

    System.setOut(System.out);
  }

  @Test
  public void finishRoundLose() {
    OutputStream messageToUser = new ByteArrayOutputStream();
    System.setOut(new PrintStream(messageToUser));

    FinishRoundRequest finishRoundRequest = new FinishRoundRequest(1, false);
    client.finishRound(finishRoundRequest);
    assertEquals("Round finishedYou lose", messageToUser.toString().trim().replace("\n", "").replace("\r", ""));

    System.setOut(System.out);
  }

  @Test
  public void testTimeout() {
    double allowedMistake = 0.0001;

    long start = System.currentTimeMillis();
    PlayerAction playerAction = client.getActionFromPlayer();
    long end = System.currentTimeMillis();

    double diff = (end-start)/1000;

    assertEquals(null, playerAction);
    assertTrue(diff >= roundduration-allowedMistake && diff <= roundduration+allowedMistake);
  }

  private void getInputFromUser(String userInput)  {
    InputStream inputFromUser = new ByteArrayInputStream(userInput.getBytes());
    System.setIn(inputFromUser);
  }

  private PlayerAction stringToPlayerAction(String s) {
    switch (s)  {
      case "e":
        return PlayerAction.EQUALS;
      case "l":
        return PlayerAction.LOWER;
      case "h":
        return PlayerAction.HIGHER;
      case "q":
        return PlayerAction.QUIT;
      default:
        return null;
    }
  }

  @Test
  public void testAllValidInputs()  {
    List<String> validInputs = new ArrayList<>(Arrays.asList("e", "l", "h", "q"));

    PlayerAction playerAction;
    for (String input : validInputs) {
      getInputFromUser(input);
      playerAction = client.getActionFromPlayer();
      assertEquals(stringToPlayerAction(input), playerAction);
    }

    System.setIn(System.in);
  }

  @Test
  public void testInvalidInputs() {
    List<String> invalidInputs = new ArrayList<>(Arrays.asList("", "ee", "\n", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"));

    PlayerAction playerAction;
    for (String input : invalidInputs) {
      getInputFromUser(input);
      playerAction = client.getActionFromPlayer();
      assertEquals(stringToPlayerAction(input), playerAction);
    }

    System.setIn(System.in);
  }
}
